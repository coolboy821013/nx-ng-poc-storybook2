import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'nx-ng-poc-stroybook-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
  encapsulation: ViewEncapsulation.Emulated
})
export class CardComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
