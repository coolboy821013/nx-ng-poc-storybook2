import { moduleMetadata, Story, Meta } from '@storybook/angular';
import { SidenavComponent } from './sidenav.component';

import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { SharedMaterialModule } from '@angular-storybook-import-scss-poc/shared/material';

export default {
  title: 'Layout/SidenavComponent',
  component: SidenavComponent,
  argTypes: {
    typeMode: {
      options: ['side', 'over', 'push'],
      control: { type: 'select' }
    },
    hasBackdrop: {
      control: { type: 'boolean' }
    }
  },
  decorators: [
    moduleMetadata({
      imports: [
        CommonModule,
        SharedMaterialModule,
        NoopAnimationsModule,
        BrowserAnimationsModule
      ],
    })
  ],
} as Meta<SidenavComponent>;

const SidenavTemplate: Story<SidenavComponent> = (args: SidenavComponent) => ({
  props: args,
});


export const Sidenav = SidenavTemplate.bind({});
Sidenav.args = {

}
