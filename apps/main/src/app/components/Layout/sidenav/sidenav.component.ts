import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'nx-ng-poc-stroybook-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss'],
  encapsulation: ViewEncapsulation.Emulated
})
export class SidenavComponent implements OnInit {

  showFiller = false;
  @Input()
  typeMode: 'side' | 'over' | 'push' = 'push';

  @Input()
  hasBackdrop = false;

  constructor() { }

  ngOnInit(): void {
  }

}
