import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'nx-ng-poc-stroybook-button-group',
  templateUrl: './button-group.component.html',
  styleUrls: ['./button-group.component.scss'],
  encapsulation: ViewEncapsulation.Emulated
})
export class ButtonGroupComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
