import { moduleMetadata, Story, Meta } from '@storybook/angular';
import { ButtonGroupComponent } from './button-group.component';
import { ButtonComponent } from '../../atoms/button/button.component';
import { SvgIconsModule } from '@ngneat/svg-icon';
import { tailwindIcon } from '../../../svg/tailwind';
import { angularIcon } from '../../../svg/angular';
import { CommonModule } from '@angular/common';

export default {
  title: 'Molecules/ButtonGroup',
  component: ButtonGroupComponent,
  decorators: [
    moduleMetadata({
      imports: [
        CommonModule,
        SvgIconsModule.forRoot({
          sizes: {
            sm: '12px',
            md: '20px',
            lg: '24px',
            xl: '48px',
          },
          defaultSize: 'xl',
          icons: [angularIcon, tailwindIcon],
        }),
      ],
      declarations: [ButtonGroupComponent, ButtonComponent],
    }),
  ],
} as Meta<ButtonGroupComponent>;

const ButtonGroupTemplate: Story<ButtonGroupComponent> = (args: ButtonGroupComponent) => ({
  props: args,
});


export const ButtonGroup = ButtonGroupTemplate.bind({});
ButtonGroup.storyName = 'Button Group';
