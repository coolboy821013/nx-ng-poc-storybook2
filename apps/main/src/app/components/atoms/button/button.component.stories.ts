import { moduleMetadata, Story, Meta } from '@storybook/angular';
import { ButtonComponent } from './button.component';
import { SvgIconsModule } from '@ngneat/svg-icon';
import { angularIcon } from '../../../svg/angular';
import { tailwindIcon } from '../../../svg/tailwind';
import { CommonModule } from '@angular/common';

export default {
  title: 'Atoms/Button',
  component: ButtonComponent,
  // Controls 相關顯示type調整
  argTypes: {
    /**ICON 設置**/
    size: {
      options: ['sm', 'md', 'lg', 'xl'],
      control: { type: 'select' },
      description: '設置ICON大小',
    },
    /**字型大小 設置**/
    fontSize: {
      control: { type:  'number'},
      description: '設置字行大小',
    },
    /** 按鈕背景顏色 設置 */
    backgroundColor: {
      control: 'color',
      description: '背景顏色設定',
    },
    /**點擊事件*/
    onButton: {
      action: 'click',
    },
  },
  decorators: [
    moduleMetadata({
      imports: [
        CommonModule,
        // 將Materital 相關Module元件匯入近來
        // MaterialModule,
        SvgIconsModule.forRoot({
          sizes: {
            sm: '12px',
            md: '20px',
            lg: '24px',
            xl: '48px',
          },
          defaultSize: 'xl',
          icons: [angularIcon, tailwindIcon],
        }),
      ],
    })
  ],
} as Meta<ButtonComponent>;

// 預設的 Template
const Template: Story<ButtonComponent> = (args: ButtonComponent) => ({
  props: args,
});

export const Primary = Template.bind({});
Primary.storyName = 'Primary Button';
Primary.args = {
  label: 'Primary Button',
  size: 'sm',
  fontSize: 18,
  backgroundColor: '#0000',
}


//////////////////////////////////////////////////////////////////////////


// Tailwind button template
const TailwindButtonTemplate: Story<ButtonComponent> = (args: ButtonComponent) => ({
  props: { ...args },
  template: `<nx-ng-poc-stroybook-button [icon]="'tailwind'" [size]="size">Tailwind</nx-ng-poc-stroybook-button>`,
});

export const TailwindButton = TailwindButtonTemplate.bind({});
TailwindButton.storyName = 'Tailwind Button';
TailwindButton.args = {
  label: 'Tailwind Button',
  size: 'lg',
  fontSize: 18,
  backgroundColor: '#0000',
};
