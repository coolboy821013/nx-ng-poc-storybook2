import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'nx-ng-poc-stroybook-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
  encapsulation: ViewEncapsulation.Emulated
})
export class ButtonComponent implements OnInit {

  @Input()
  icon = 'angular';

  @Input()
  size: 'sm' | 'md' | 'lg' | 'xl' = 'xl';

  @Input()
  fontSize = 16;

  @Input()
  label = 'Button Demo';

  @Input()
  backgroundColor = '#0000';

  constructor() { }

  ngOnInit(): void {
  }

  onButton(e: any) {
    console.log(e);
  }

}
