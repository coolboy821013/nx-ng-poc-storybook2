import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'nx-ng-poc-stroybook-button-section',
  templateUrl: './button-section.component.html',
  styleUrls: ['./button-section.component.scss'],
  encapsulation: ViewEncapsulation.Emulated
})
export class ButtonSectionComponent implements OnInit {

  @Input()
  backgroundcolor = "#FFFF";

  constructor() { }

  ngOnInit(): void {
  }

}
