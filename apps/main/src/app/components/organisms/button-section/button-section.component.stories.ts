import { moduleMetadata, Story, Meta } from '@storybook/angular';
import { ButtonSectionComponent } from './button-section.component';
import { angularIcon } from '../../../svg/angular';
import { tailwindIcon } from '../../../svg/tailwind';
import { SvgIconsModule } from '@ngneat/svg-icon';
import { ButtonGroupComponent } from '../../molecules/button-group/button-group.component';
import { ButtonComponent } from '../../atoms/button/button.component';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';

export default {
  title: 'Organisms/ButtonSection',
  component: ButtonSectionComponent,
  argTypes: {
    backgroundColor: { control: 'color' },
  },
  decorators: [
    moduleMetadata({
      imports: [
        CommonModule,
        BrowserAnimationsModule,
        NoopAnimationsModule,
        SvgIconsModule.forRoot({
          sizes: {
            sm: '12px',
            md: '20px',
            lg: '24px',
            xl: '48px',
          },
          defaultSize: 'xl',
          icons: [angularIcon, tailwindIcon],
        }),
      ],
      declarations: [ButtonGroupComponent, ButtonSectionComponent, ButtonComponent],
    })
  ],
} as Meta<ButtonSectionComponent>;

const ButtonSectionTemplate: Story<ButtonSectionComponent> = (args: ButtonSectionComponent) => ({
  props: args,
  // props: { ...args },
  // template: `<nx-ng-poc-stroybook-button-section></nx-ng-poc-stroybook-button-section>`,
});

export const ButtonSection = ButtonSectionTemplate.bind({});
ButtonSection.args = {
}
ButtonSection.storyName = "Button Section"
