import { Component } from '@angular/core';

@Component({
  selector: 'angular-storybook-import-scss-poc-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'main';
}
