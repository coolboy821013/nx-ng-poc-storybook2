
// 只要匹配到 /background|color$/i 就可以自動轉換操作該 props 的方式為 { type: 'color' } ，
// 而 color type 對應的是 color picker。
export const parameters = {
  actions: { argTypesRegex: '^on.*' },
  controls: {
    expanded: true,
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
};
// export const parameters = {
//   actions: { argTypesRegex: '^on.*' }
// }
